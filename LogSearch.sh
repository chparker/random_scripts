#Log Search for KeyWord
echo
Date

current="$(pwd)"
echo
echo "You are currently in: "$current"" 
echo
echo “Enter the directory you want to search in:”
read -e dir
echo
#cd $dir
echo “Enter the string you are looking for:”
read -a string
echo
echo "Give the output file a name:"
read fname

#results=$(grep -r $string $dir)

results=$(grep -r "${string[c]}" $dir)

if [ -z "$results" ] ; then

	echo "No matches were found"

else

	echo "$results" > "$fname.txt"
fi