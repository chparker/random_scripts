## Script to pull down any version of an Atlassian product in all the forms it is available in, i.e.: OS, Bit Architecture, Installer or zip/tar

# Select the Download location

dlLoc=/Users/chparker/Documents/TempStorage

# Get the product
echo "Product to download"
read product
product=$(echo "$product" | tr '[:upper:]' '[:lower:]') > /dev/null
	if [[ ( $product == "bitbucket" || $product == "stash" ) ]]; then
		echo "From version 4+ Stash was rebranded as Bitbucket, ensure you input the correct name for the corresponding version"
	fi
echo
sleep 3s

#Get the version number
echo "Version Number"
read versionnum

# Deteremine OS
echo "windows or linux"
read os
os=$(echo "$os" | tr '[:upper:]' '[:lower:]') > /dev/null

# OS Details
	if [ $os == "windows" ]; then
		echo "zip or installer"
		read filetype
		filetype=$(echo "$filetype" | tr '[:upper:]' '[:lower:]') > /dev/null
	elif [ $os == "linux" ]; then
		if [[ ( $product == "fisheye" || $product == "crucible" ) ]]; then
			echo "Only .zip is available for Linux"
			filetype="zip"
			sleep 3s
		else
			echo "tar or installer"
			read filetype
			filetype=$(echo "$filetype" | tr '[:upper:]' '[:lower:]') > /dev/null
		fi
	fi


# Decide on architecture
                if [ $filetype == "installer" ]; then
			echo "64 bit or 32 bit"
			read bit
		fi

#Create the extension from the filetype input
        if [ $filetype == "zip" ]; then
		extension=".zip"
	elif [ $filetype == "tar" ]; then
		extension=".tar.gz"
	elif [ $os == "windows" ] && [ $filetype == "installer" ] && [ $bit == "64" ]; then
		extension="-x64.exe"
	elif [ $os == "windows" ] && [ $filetype == "installer" ] && [ $bit == "32" ]; then
		extension="-x32.exe"
	elif [ $os == "linux" ] && [ $filetype == "installer" ] && [ $bit == "64" ]; then
		extension="-x64.bin"
	elif [ $os == "linux" ] && [ $filetype = "installer" ] && [ $bit == "32" ]; then
		extension="-x32.bin"
	fi

# Force lowercase for filetype variable

#filetype=$(echo "$filetype" | tr '[:upper:]' '[:lower:]') > /dev/null

# Build URL

if [[ ( $product == "fisheye" || $product == "crucible" ) && ( $filetype == "zip" ) ]]; then

	url="https://www.atlassian.com/software/$product/downloads/binary/$product-$versionnum$extension"
else
	url="https://www.atlassian.com/software/$product/downloads/binary/atlassian-$product-$versionnum$extension"
fi

# Output for user

echo "This is the URL to be downloaded along with the choices made:"
echo
echo "______Your_Choices_______"
echo $product
echo $versionnum
echo $os
echo $filetype
echo $bit
echo "_________________________"
echo
echo $url
echo
# Have the user confirm if they are happy with choices, if not restart script
read -p "Continue with the download y/n?" -n 1 -r
echo	
	if [[ $REPLY =~ ^[Yy]$ ]]; then
		wget -P $dlLoc $url
	else
		sh versionDownload.sh
	fi
echo
echo
