#Script to manually search through the SupportAttachments folder and remove any .zip files

#Get the count for the number of each products zips

function count {
fish=$(find ~/Documents/SupportAttachments -name "FishEye_support*.zip" | wc -l)
stash=$(find ~/Documents/SupportAttachments -name "Stash_support*.zip" | wc -l)
bit=$(find ~/Documents/SupportAttachments -name "Bitbucket_support*.zip" | wc -l)
jira=$(find ~/Documents/SupportAttachments -name "JIRA_support*.zip" | wc -l)
}

#Call count

count

#Echo the amount for each
echo
echo "Fecru zips = 	$fish"
echo "Stash zips = 	$stash" 
echo "BB zips = 	$bit"
echo "JIRA zips = 	$jira" 

#Remove all the zips
echo
echo "Deleting all of the zips"
echo
function delete {
find ~/Documents/SupportAttachments -name "FishEye_support*.zip" -delete
find ~/Documents/SupportAttachments -name "Stash_support*.zip" -delete
find ~/Documents/SupportAttachments -name "Bitbucket_support*.zip" -delete
find ~/Documents/SupportAttachments -name "JIRA_support*.zip" -delete
}

#Call the delete function

delete

#Run the count funciton again

count

#Show the count again 
echo
echo "Fecru zips = 	$fish"
echo "Stash zips = 	$stash"
echo "BB zips =       $bit"
echo "JIRA zips = 	$jira"
echo
