# Small shell script to remove all screenshots from the Desktop

# Count the amount of screenshots present
QUANT=`ls ~/Desktop/Screen\ Shot\ * | wc -l`

echo "The total number of Screen Shot files to be removed is" $QUANT

#Remove all of these ScreenShot files
rm ~/Desktop/Screen\ Shot\ *


