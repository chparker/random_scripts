# Script which lets the user pick which applications to start up

current="$(pwd)"

##Confirmation that the services are running - status functions

#Crowd

crowdrun () {
	PID=`ps aux | grep java | grep crowd | awk '{print $2}'`
		if test -z $PID
		then
			echo "Crowd is down..."
		else
			echo "Crowd is running...PID $PID"
		fi

	sleep 5
}

#JIRA

jirarun () {
	PID=`ps aux | grep java | grep jira | awk '{print $2}'`
        	if test -z $PID
        	then
        		echo "JIRA is down..."
        	else
        		echo "JIRA is running... PID $PID"
        	fi

	sleep 5
}

#FishEye

fishrun () {
	PID=`ps aux | grep java | grep fisheye | awk '{print $2}'`
        	if test -z $PID
       		then
        		echo "FishEye is down..."
        	else
        		echo "FishEye is running... PID $PID"
        	fi

	sleep 5
}

#Stash

stashrun () {
	PID=`ps aux | grep java | grep stash | awk '{print $2}'`
        	if test -z $PID
        	then
        		echo "Stash is down..."
        	else
        		echo "Stash is running... PID $PID"
        	fi

	sleep 5
}

#Confluence

confrun () {
	PID=`ps aux | grep java | grep confluence | awk '{print $2}'`
        	if test -z $PID
        	then
        		echo "Confluence is down..."
        	else
        		echo "Confluence is running... PID $PID"
        	fi

	}



#Appplication Startup functions

#Crowd
crowd () {
	cd ~/Documents/Atlassian/Crowd/atlassian-crowd-2.8.0/
		echo "$current"
		echo
		read -p "We will now start Crowd - press any key to continue"
		sh start_crowd.sh
		echo
	sleep 15
		crowdrun
}	

#JIRA
jira () {
	cd ~/Documents/Atlassian/NewJIRA/atlassian-jira-6.4.12-standalone/bin
		echo "$current"
		echo
		read -p "We will now start JIRA - Press any key to continue"
		echo
		sh start-jira.sh
		echo
	sleep 15
		jirarun
}

#FishEye

fisheye () {
	cd ~/Documents/Atlassian/FisheyeCrucible/fecru-3.8.1/bin
		echo "$current"
		echo
		read -p "We will now start FishEye - Press any key to continue"
		echo
		sh start.sh --quiet
		echo
	sleep 15
		fishrun
}

#Stash

stash () {
	cd ~/Documents/Atlassian/Stash/3.11.1/bin/
		echo "$current"
		echo
		read -p "We will now start Stash - Press any key to continue"
		echo
		sh start-stash.sh
		echo
	sleep 15
		stashrun
}

#Confluence

conf () {
	cd ~/Documents/Atlassian/Conf/atlassian-confluence-5.7.4/bin/
		echo "$current"
		echo
		read -p "We will now start Confluence - Press any key to continue"
		echo
		sh start-confluence.sh
		echo
	sleep 15
		confrun
}

#Joint application startup

startAll () {

	crowd
	sleep 15
	jira
	sleep 15
	fisheye
	sleep 15
	stash 
	sleep 15
	conf
	sleep 15
}


#	testfunc () {

#	touch ~/Desktop/testFile.txt
#}

#Prompt the user to pick which applications they want to start

echo "Please enter the number(s) that represent the applicaitons you wish to start up:"
echo "1 - Crowd, 2 - JIRA, 3 - FishEye, 4 - Stash, 5 - Confluence, 6 - All"
read appChoice

case $appChoice in
	[1]* ) crowd
		;;
	[2]* ) jira
		;;
	[3]* ) fisheye
		;;
	[4]* ) stash
		;;
	[5]* ) conf
		;;
	[6]* ) startAll
		;;
esac
