#Stop up all of the Atlassian tools

current="$(pwd)"

#Crowd
	cd ~/Documents/Atlassian/Crowd/atlassian-crowd-2.8.0/
		echo "$current"
		echo read -p "Stopping Crowd - Press any key to continue"
		echo
		sh stop_crowd.sh
		echo
	sleep 15

#JIRA

	cd ~/Documents/Atlassian/NewJIRA/atlassian-jira-6.4.12-standalone/bin/
		echo "$current"
		echo
		read -p "Stopping JIRA - Press any key to continue"
		echo
		sh stop-jira.sh
		echo
	sleep 15

#FishEye

	cd ~/Documents/Atlassian/FisheyeCrucible/fecru-3.8.1/bin
		echo "$current"
		echo
		read -p "Stopping FishEye - Press any key to continue"
		echo
		sh stop.sh --quiet
		echo
	sleep 15

#Stash

	cd ~/Documents/Atlassian/Stash/3.11.1/bin/
		echo "$current"
		echo
		read -p "Stopping Stash - Press any key to continue"
		echo
		sh stop-stash.sh
		echo
	sleep 15

#Confluence

	cd ~/Documents/Atlassian/Conf/atlassian-confluence-5.7.4/bin/
		echo "$current"
		echo
		read -p "Stopping Confluence - Press any key to continue"
		echo
		sh stop-confluence.sh
		echo
	sleep 15

##Confirmation that the services are running

#Crowd
	
	PID=`ps aux | grep java | grep crowd | awk '{print $2}'`
		if test -z $PID
		then
			echo "Crowd is down..."
		else
			echo "Crowd is still running... $PID"
		fi

	sleep 5

#JIRA

	PID=`ps aux | grep java | grep jira | awk '{print $2}'`
        	if test -z $PID
        	then
        		echo "JIRA is down..."
        	else
        		echo "JIRA is still running... PID $PID"
        	fi

	sleep 5

#FishEye

	PID=`ps aux | grep java | grep fisheye | awk '{print $2}'`
        	if test -z $PID
       		then
        		echo "FishEye is down..."
        	else
        		echo "FishEye is still running... PID $PID"
        	fi

	sleep 5

#Stash

	PID=`ps aux | grep java | grep stash | awk '{print $2}'`
        	if test -z $PID
        	then
        		echo "Stash is down..."
        	else
        		echo "Stash is still running... PID $PID"
        	fi

	sleep 5

#Confluence

	PID=`ps aux | grep java | grep confluence | awk '{print $2}'`
        	if test -z $PID
        	then
        		echo "Confluence is down..."
        	else
        		echo "Confluence is still running... PID $PID"
        	fi


